package com.cerveza.togglefunction;

import com.cerveza.togglefunction.api.ControlFunction;
import com.cerveza.togglefunction.api.EMessageType;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by cerveza on 04/02/2017.
 */

public class SharedConstants {
    public static final String MESSAGE_PATH = "/com/cerveza/togglefunction";
    public static final String MESSAGE_PATH_TOGGLES = "/com/cerveza/togglefunction/toggles";
    public static final String MESSAGE_PATH_OVERVIEW = "/com/cerveza/togglefunction/overview";
    public static final String MESSAGE_PATH_OTHERS = "/com/cerveza/togglefunction/others";

    public static final List<ControlFunction> functionControls = Arrays.asList(
            new ControlFunction(EMessageType.WIFI_CHANGE_STATUS, "WiFi", 0),
            new ControlFunction(EMessageType.BLUETOOTH_CHANGE_STATUS, "Bluetooth", 0),
            new ControlFunction(EMessageType.MOBILE_DATA_CHANGE_STATUS, "Mobile data", 0),
            new ControlFunction(EMessageType.GPS_CHANGE_STATUS, "GPS", 0),
            new ControlFunction(EMessageType.AUDIO_CHANGE_STATUS, "Audio", 0));

    public static List<ControlFunction> getFunctionControls(){
        List<ControlFunction> list =  new ArrayList();
        for (ControlFunction function : functionControls) {
            list.add(function);
        }
        return list;
    }


}

