package com.cerveza.togglefunction.api;

import java.io.Serializable;

/**
 * Created by cerveza on 14/11/2016.
 */

public class ControlFunction implements Serializable {
    private EMessageType ID;
    private String name;
    private int state; //0- off, 1-on, 2-vibration

    public ControlFunction(EMessageType ID, String name, int state) {
        this.ID = ID;
        this.name = name;
        this.state = state;
    }

    public void setDefaultState(){
        state = 0;
    }

    public EMessageType getID() {
        return ID;
    }

    public String getName() {
        return name;
    }

    public int getState() {
        return state;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ControlFunction that = (ControlFunction) o;

        if (state != that.state) return false;
        if (ID != that.ID) return false;
        return name != null ? name.equals(that.name) : that.name == null;
    }

    @Override
    public int hashCode() {
        int result = ID != null ? ID.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }
}
