package com.cerveza.togglefunction.api;

import java.io.Serializable;

/**
 * Created by cerveza on 20/11/2016.
 */

public class MyWifiInfo implements Serializable {
    String bssid;
    int ipAddress;
    int linkSpeed;
    String macAddress;
    int rssi;
    String ssid;

    public MyWifiInfo(String bssid, int ipAddress, int linkSpeed, String macAddress, int rssi, String ssid){
        this.bssid = bssid;
        this.ipAddress = ipAddress;
        this.linkSpeed = linkSpeed;
        this.macAddress = macAddress;
        this.rssi = rssi;
        this.ssid = ssid;
    }

    public String getBssid() {
        return bssid;
    }

    public String getIpAddress() {
        return String.format(
                "%d.%d.%d.%d",
                (ipAddress & 0xff),
                (ipAddress >> 8 & 0xff),
                (ipAddress >> 16 & 0xff),
                (ipAddress >> 24 & 0xff));
    }

    public String getLinkSpeed() {
        return String.format("%d Mbps",linkSpeed);
    }

    public String getMacAddress() {
        return macAddress;
    }

    public String getRssi() {
        return String.format("%d dBm", rssi);
    }

    public String getSsid() {
        return ssid;
    }

}
