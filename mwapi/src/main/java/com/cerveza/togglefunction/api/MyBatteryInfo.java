package com.cerveza.togglefunction.api;

import java.io.Serializable;

/**
 * Created by cerveza on 04.03.2017.
 */

public class MyBatteryInfo implements Serializable {
    private int capacity;
    private int status;
    private int health;
    private int powerSource;

    public MyBatteryInfo(int capacity, int status, int health, int powerSource) {
        this.capacity = capacity;
        this.status = status;
        this.health = health;
        this.powerSource = powerSource;
    }

    public int getCapacity() {
        return capacity;
    }

    public int getStatus() {
        return status;
    }

    public int getHealth() {
        return health;
    }

    public int getPowerSource() {
        return powerSource;
    }
}
