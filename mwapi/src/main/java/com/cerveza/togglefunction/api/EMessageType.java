package com.cerveza.togglefunction.api;

import com.cerveza.togglefunction.SharedConstants;

/**
 * Created by cerveza on 14/11/2016.
 */

public enum EMessageType {
    //FUNCTIONS
    WIFI_CHANGE_STATUS(SharedConstants.MESSAGE_PATH_TOGGLES+"/wifi_change_status"),
    BLUETOOTH_CHANGE_STATUS(SharedConstants.MESSAGE_PATH_TOGGLES+"/bluetooth_change_status"),
    GPS_CHANGE_STATUS(SharedConstants.MESSAGE_PATH_TOGGLES+"/gps_change_status"),
    MOBILE_DATA_CHANGE_STATUS(SharedConstants.MESSAGE_PATH_TOGGLES+"/mobile_data_change_status"),
    AUDIO_CHANGE_STATUS(SharedConstants.MESSAGE_PATH_TOGGLES+"audio_change_status"),

    //OTHERS
    FUNCTIONS_STATE_SYNCHRONIZATION(SharedConstants.MESSAGE_PATH_TOGGLES+"/function_synchronization"),
    FUNCTIONS_STATE_INITIALIZE(SharedConstants.MESSAGE_PATH_TOGGLES+"/function_state_initialize"),

    //Overview
    GET_WIFI_INFO(SharedConstants.MESSAGE_PATH_OVERVIEW+"/get_wifi_info"),
    OVERVIEW_UPDATE_BATTERY(SharedConstants.MESSAGE_PATH_OVERVIEW+"/battery_update"),;

    public String path;

    EMessageType(String path){
        this.path = path;
    }

    public String getPath(){
        return this.path;
    }


}
