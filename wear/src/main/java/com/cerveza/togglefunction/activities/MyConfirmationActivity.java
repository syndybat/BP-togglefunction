package com.cerveza.togglefunction.activities;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.wearable.view.DelayedConfirmationView;
import android.view.View;
import android.widget.TextView;

import com.cerveza.togglefunction.R;

public class MyConfirmationActivity extends Activity implements DelayedConfirmationView.DelayedConfirmationListener{
    String message = "Really?";
    private DelayedConfirmationView mDelayedView;
    private Intent intentCallback;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirmation);

        Intent intent = getIntent();
        if(intent.hasExtra("COMMAND")){
            intentCallback  = (Intent) intent.getExtras().get("COMMAND");
        }
        if(intent.hasExtra("MESSAGE")){
            message = intent.getStringExtra("MESSAGE");
        }

        mDelayedView =
                (DelayedConfirmationView) findViewById(R.id.delayed_confirmation);
        mDelayedView.setListener(this);

        TextView textView = (TextView) findViewById(R.id.linear_layout_confirmation).findViewById(R.id.textViewMessage);
        textView.setText(message);

        mDelayedView.setTotalTimeMs(3000);
        mDelayedView.start();

    }

    @Override
    public void onTimerFinished(View view) {
        mDelayedView.reset();
        finish();
    }

    @Override
    public void onTimerSelected(View view) {
        mDelayedView.reset();
        if(intentCallback !=null){
            this.startService(intentCallback);
        }
        finish();
    }
}
