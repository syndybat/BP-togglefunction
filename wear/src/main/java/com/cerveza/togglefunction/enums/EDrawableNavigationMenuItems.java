package com.cerveza.togglefunction.enums;

/**
 * Created by cerveza on 23/02/2017.
 */

public enum EDrawableNavigationMenuItems {
    OVERVIEW, TOGGLES, OTHERS
}
