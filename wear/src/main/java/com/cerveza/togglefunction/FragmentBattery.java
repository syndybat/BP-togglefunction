package com.cerveza.togglefunction;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.BatteryManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.cerveza.togglefunction.api.EMessageType;
import com.cerveza.togglefunction.api.MyBatteryInfo;
import com.cerveza.togglefunction.serializeutil.SerializeUtils;
import com.google.android.gms.wearable.MessageEvent;

import java.io.IOException;

/**
 * Created by cerveza on 04.03.2017.
 */

public class FragmentBattery extends Fragment {
    TextView tvCapacity;
    TextView tvHealth;
    TextView tvStatus;
    TextView tvPowerSource;

    MyBatteryInfo myBatteryInfo;
    boolean isFocused;

    public void onAttach(Context context) {
        /*this.context = context;
        mListener = (OnFragmentSwitchListener) getActivity();*/
        super.onAttach(context);
    }

    @Override
    public void onAttach(Activity activity) {
       /* this.context = activity.getApplicationContext();
        mListener = (OnFragmentSwitchListener) getActivity();*/
        super.onAttach(activity);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        isFocused = false;
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        isFocused = true;
        updateUI();
    }

    @Override
    public void onPause() {
        super.onPause();
        isFocused = false;
    }

    private void updateUI(){
        if(myBatteryInfo == null)
            return;

        tvCapacity.setText(myBatteryInfo.getCapacity() + " %");
        tvHealth.setText(getHealthString(myBatteryInfo.getHealth()));
        tvStatus.setText(getStatusString(myBatteryInfo.getStatus()));
        tvPowerSource.setText(getPowerSourceString(myBatteryInfo.getPowerSource()));
    }

    private String getPowerSourceString(int index){
        switch (myBatteryInfo.getPowerSource()){
            case BatteryManager.BATTERY_PLUGGED_AC: return "Socket";
            case BatteryManager.BATTERY_PLUGGED_USB: return "USB";
            case BatteryManager.BATTERY_PLUGGED_WIRELESS: return "Wireless";
            default: return "None";
        }
    }

    private String getStatusString(int index){
        switch (index){
            case BatteryManager.BATTERY_STATUS_CHARGING: return "Charging";
            case BatteryManager.BATTERY_STATUS_DISCHARGING: return "Discharging";
            case BatteryManager.BATTERY_STATUS_FULL: return "Full";
            case BatteryManager.BATTERY_STATUS_NOT_CHARGING: return "Not charging";
            case BatteryManager.BATTERY_STATUS_UNKNOWN: return "Unknown";
            default: return "N/A";
        }
    }

    private String getHealthString(int index){
        switch (index){
            case BatteryManager.BATTERY_HEALTH_COLD: return "Cold";
            case BatteryManager.BATTERY_HEALTH_DEAD: return "Dead";
            case BatteryManager.BATTERY_HEALTH_GOOD: return "Good";
            case BatteryManager.BATTERY_HEALTH_OVER_VOLTAGE: return "Over voltage";
            case BatteryManager.BATTERY_HEALTH_UNKNOWN: return "Unknown";
            case BatteryManager.BATTERY_HEALTH_OVERHEAT: return "Overheat";
            case BatteryManager.BATTERY_HEALTH_UNSPECIFIED_FAILURE: return "Not specified";
            default: return "N/A";
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_battery, container, false);
        tvCapacity = (TextView) view.findViewById(R.id.capacity);
        tvHealth = (TextView) view.findViewById(R.id.health);
        tvStatus = (TextView) view.findViewById(R.id.status);
        tvPowerSource = (TextView) view.findViewById(R.id.power_source);

        return view;
    }

    public void handleMessage(MessageEvent messageEvent){
        if(messageEvent.getPath().equalsIgnoreCase(EMessageType.OVERVIEW_UPDATE_BATTERY.getPath())){
            myBatteryInfo = (MyBatteryInfo) deserializeData(messageEvent.getData());

            if (isFocused)
                updateUI();
        }
    }

    private Object deserializeData(byte[] bytes){
        Object object;
        try {
            object = SerializeUtils.convertFromBytes(bytes);
            return object;
        } catch (IOException e) {
            Log.e(getClass().getName(),"IO exeption during converting from byte[] to object",e);
        } catch (ClassNotFoundException e) {
            Log.e(getClass().getName(),"deserilization exception could not find class ControlFunction",e);
        }
        return null;
    }
}
