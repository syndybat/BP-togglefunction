package com.cerveza.togglefunction.interfaces;

/**
 * Created by cerveza on 23/02/2017.
 */

public interface IOnNavigationChange {
        void onNavigationChange(int pos);
}
