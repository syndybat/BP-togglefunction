package com.cerveza.togglefunction;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.cerveza.togglefunction.api.EMessageType;
import com.cerveza.togglefunction.api.MyWifiInfo;
import com.cerveza.togglefunction.serializeutil.SerializeUtils;
import com.google.android.gms.wearable.MessageEvent;

import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;


public class FragmentWifiInfo extends Fragment {
    Timer timer;
    Context context;

    TextView ssid;
    TextView bssid;
    TextView ipAddress;
    TextView macAddress;
    TextView linkSpeed;
    TextView signalStrength;

    MyWifiInfo wifiInfo;
    boolean isFocused;


    public FragmentWifiInfo() {
        // Required empty public constructor
    }

    public void onAttach(Context context) {
        this.context = context;
        //mListener = (OnFragmentSwitchListener) getActivity();*/
        super.onAttach(context);
    }

    @Override
    public void onAttach(Activity activity) {
        this.context = activity.getApplicationContext();
        //mListener = (OnFragmentSwitchListener) getActivity();*/
        super.onAttach(activity);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        isFocused = false;
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                Intent intent = new Intent(context,MessageSendServiceWear.class).setAction("SEND_MESSAGE").
                        putExtra("MESSAGE_PATH", EMessageType.GET_WIFI_INFO.getPath());
                context.startService(intent);
            }
        }, 0, 2000);
        isFocused = true;
        updateView();
    }

    @Override
    public void onPause() {
        super.onPause();
        isFocused = false;

        timer.cancel();
        timer.purge();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_wifi_info, container, false);

        ssid = ((TextView)view.findViewById(R.id.textViewSsid));
        bssid = ((TextView)view.findViewById(R.id.textViewBssid));
        ipAddress = ((TextView)view.findViewById(R.id.textViewIpaddress));
        macAddress = ((TextView)view.findViewById(R.id.textViewMacaddress));
        linkSpeed = ((TextView)view.findViewById(R.id.textViewLinkspeed));
        signalStrength = ((TextView)view.findViewById(R.id.textViewSignalstrength));

        return view;
    }

    private void updateView(){
        if(wifiInfo == null)
            return;

        ssid.setText(wifiInfo.getSsid());
        bssid.setText(wifiInfo.getBssid());
        ipAddress.setText(wifiInfo.getIpAddress());
        macAddress.setText(wifiInfo.getMacAddress());
        linkSpeed.setText(wifiInfo.getLinkSpeed());
        signalStrength.setText(wifiInfo.getRssi());
    }

    public void handleMessage(MessageEvent messageEvent){
        if(messageEvent.getPath().equalsIgnoreCase(EMessageType.GET_WIFI_INFO.getPath())){
           wifiInfo = (MyWifiInfo) deserializeData(messageEvent.getData());
        }

        if(isFocused){
            updateView();
        }
    }

    private Object deserializeData(byte[] bytes){
        Object object;
        try {
            object = SerializeUtils.convertFromBytes(bytes);
            return object;
        } catch (IOException e) {
            Log.e(getClass().getName(),"IO exeption during converting from byte[] to object",e);
        } catch (ClassNotFoundException e) {
            Log.e(getClass().getName(),"deserilization exception could not find class ControlFunction",e);
        }
        return null;
    }
}
