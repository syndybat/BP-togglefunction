package com.cerveza.togglefunction.com.cerveza.togglefunction.ui;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.support.wearable.activity.ConfirmationActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.cerveza.togglefunction.MessageSendServiceWear;
import com.cerveza.togglefunction.activities.MapsActivity;
import com.cerveza.togglefunction.activities.MyConfirmationActivity;
import com.cerveza.togglefunction.R;
import com.cerveza.togglefunction.api.ControlFunction;
import com.cerveza.togglefunction.api.EMessageType;

import java.util.List;

/**
 * Created by cerveza on 14/11/2016.
 */

public class Adapter extends RecyclerView.Adapter {
    private List<ControlFunction> mDataset;
    private final Context mContext;
    private final LayoutInflater mInflater;

    // Provide a suitable constructor (depends on the kind of dataset)
    public Adapter(Context context, List<ControlFunction> dataset) {
        mContext = context;
        mInflater = LayoutInflater.from(context);
        mDataset = dataset;

    }

    public void refresh(List<ControlFunction> mDataset)
    {
        this.mDataset = mDataset;
        notifyDataSetChanged();
    }


    // Provide a reference to the type of views you're using
    private class ItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener
    {
        private ImageView imageView;
        public ItemViewHolder(View itemView) {
            super(itemView);
            imageView = (ImageView) itemView.findViewById(R.id.circle);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            Integer tag = (Integer) v.getTag();
            EMessageType messageType =  mDataset.get(tag).getID();

            if(messageType == EMessageType.BLUETOOTH_CHANGE_STATUS || messageType == EMessageType.WIFI_CHANGE_STATUS){
                if(mDataset.get(tag).getState() == 1){
                    alertDialog("Really? you may lost your connection", messageType.getPath());
                    return;
                }
            }

            if(mDataset.get(tag).getID() == EMessageType.GPS_CHANGE_STATUS){
                Intent intent = new Intent(mContext, ConfirmationActivity.class);
                intent.putExtra(ConfirmationActivity.EXTRA_ANIMATION_TYPE,
                        ConfirmationActivity.SUCCESS_ANIMATION);
                intent.putExtra(ConfirmationActivity.EXTRA_MESSAGE,
                        "Finish the task on the phone please");
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                mContext.startActivity(intent);
            }

            Intent intent = new Intent(mContext,MessageSendServiceWear.class).setAction("SEND_MESSAGE").
                            putExtra("MESSAGE_PATH",messageType.getPath());
            mContext.startService(intent);
        }
    }

    public void alertDialog(String message,String messagePath){

        Intent intent = new Intent(mContext, MyConfirmationActivity.class)
                .putExtra("COMMAND",new Intent(mContext,MessageSendServiceWear.class).setAction("SEND_MESSAGE").
                        putExtra("MESSAGE_PATH",messagePath))
                .putExtra("MESSAGE",message);
        mContext.startActivity(intent);
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ItemViewHolder(mInflater.inflate(R.layout.list_item, null));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ItemViewHolder itemHolder = (ItemViewHolder) holder;

        String imageName = mDataset.get(position).getName().toLowerCase().replace(" ","_");
        switch (mDataset.get(position).getState()){
            case 0:
                imageName = imageName.concat("_off");
                break;
            case 1:
                imageName = imageName.concat("_on");
                break;
            case 2:
                imageName = imageName.concat("_vibration");
                break;
        }
        imageName = "ic_icon_".concat(imageName);
        int resID = mContext.getResources().getIdentifier(imageName , "drawable", mContext.getPackageName());
        itemHolder.imageView.setImageResource(resID);
        holder.itemView.setTag(position);
    }

    // Return the size of your dataset
    @Override
    public int getItemCount() {
        return mDataset.size();
    }
}
