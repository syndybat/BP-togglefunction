package com.cerveza.togglefunction.com.cerveza.togglefunction.ui;

import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.GradientDrawable;
import android.support.v4.content.ContextCompat;
import android.support.wearable.view.WearableListView;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cerveza.togglefunction.R;

/**
 * Created by cerveza on 14/11/2016.
 */

public class WearableListItemLayout extends LinearLayout implements WearableListView.OnCenterProximityListener {
        private static final float FADED_TEXT_ALPHA = 0.2f;
        private ImageView mCircle;
        //private TextView mName;


        public WearableListItemLayout(Context context) {
            this(context, null);
        }

        public WearableListItemLayout(Context context, AttributeSet attrs) {
            this(context, attrs, 0);
        }

        public WearableListItemLayout(Context context, AttributeSet attrs,
        int defStyle) {
            super(context, attrs, defStyle);

        }

        // Get references to the icon and text in the item layout definition
        @Override
        protected void onFinishInflate() {
            super.onFinishInflate();
            // These are defined in the layout file for list items
            // (see next section)
            mCircle = (ImageView) findViewById(R.id.circle);
           // mName = (TextView) findViewById(R.id.name);
        }

        @Override
        public void onCenterPosition(boolean animate) {
           //mName.setAlpha(1f);
            mCircle.getLayoutParams().height = 150;
            mCircle.getLayoutParams().width = 150;
           // mCircle.requestLayout();
        }

        @Override
        public void onNonCenterPosition(boolean animate) {
            mCircle.getLayoutParams().height = 100;
            mCircle.getLayoutParams().width = 100;
            mCircle.requestLayout();
            //mName.setAlpha(FADED_TEXT_ALPHA);
        }
}
