package com.cerveza.togglefunction;

import android.util.Log;

import org.junit.Test;

import java.util.Calendar;
import java.util.Timer;
import java.util.TimerTask;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {
    @Test
    public void addition_isCorrect() throws Exception {
        Timer timer = new Timer();
        TimerTask timerTask = new TimerTask() {
            @Override
            public void run() {
                return;
            }
        };

        timer.schedule(timerTask,12356);

        long time = 0;
        for(int i = 0; i< 12356698; i++){
            if(i % 1000000 == 0)
               time = Calendar.getInstance().getTimeInMillis() - timerTask.scheduledExecutionTime();
            }
    }
}