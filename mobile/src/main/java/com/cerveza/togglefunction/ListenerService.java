package com.cerveza.togglefunction;

import android.content.Intent;
import android.util.Log;

import com.google.android.gms.wearable.MessageEvent;
import com.google.android.gms.wearable.WearableListenerService;

/**
 * Created by cerveza on 14/11/2016.
 */

public class ListenerService extends WearableListenerService{

   @Override
    public void onCreate() {
        super.onCreate();
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onMessageReceived(MessageEvent messageEvent) {
        Log.d("Message received:",messageEvent.getPath());
        if (messageEvent.getPath().contains(SharedConstants.MESSAGE_PATH)) {
            Intent intentMessage = null;
                intentMessage = new Intent(this, ToggleFunctionServer.class).setAction("WEARABLE_COMMAND")
                        .putExtra("MESSAGE_PATH", messageEvent.getPath());
                startService(intentMessage);
        }else{
                super.onMessageReceived(messageEvent);
            }
        }



}



