package com.cerveza.togglefunction;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.cerveza.togglefunction.api.EMessageType;

import java.util.Arrays;
import java.util.List;

/**
 * Created by cerveza on 17/11/2016.
 */

public class FunctionChangeReceiver extends BroadcastReceiver {

    public static final List<String> INTENT_FILTER_ARRAY = Arrays.asList("android.net.wifi.WIFI_STATE_CHANGED",
                                                                        "android.net.conn.CONNECTIVITY_CHANGE",
                                                                        "android.bluetooth.adapter.action.STATE_CHANGED",
                                                                        "android.location.PROVIDERS_CHANGED",
                                                                        "android.media.RINGER_MODE_CHANGED");

    @Override
    public void onReceive(Context context, Intent intent) {
        Intent intentSendMessage = new Intent(context, ToggleFunctionServer.class).setAction("SERVICE_COMMAND")
                .putExtra("MESSAGE_PATH", EMessageType.FUNCTIONS_STATE_SYNCHRONIZATION.getPath());
        context.startService(intentSendMessage);
    }
}
